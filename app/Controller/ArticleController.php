<?php
	App::uses('AppController','Controller');

	class ArticleController extends AppController
	{	
		public $layout = 'default_front';
		public $uses = array('Article','Categories');
		public $components = array('RequestHandler');
		public function article_detail($id){
			$articles = $this->Article->getArticleById($id);
			$this->set('articles',$articles);
			$category_id = $articles['Article']['category_id'];
			$article_related = $this->Article->getArticleRelated($category_id);
			$this->set('article_related',$article_related);
			//pr($article_related);exit;
		}
		public function list_article($id){
			$list_articles = $this->Article->getArticleByCateroryId($id, 2, 0);
			$this->set('list_articles',$list_articles);
			// pr($list_articles);exit;
			$title = $this->Categories->getNameCategoryById($id);
			$this->set('title',$title);
			$this->set('id',$id);
		}
		public function more_list_article($id, $limit, $offset){
			$this->autoRender = false;
			if($this->RequestHandler->isAjax()){
				$more_list_article = $this->Article->getArticleByCateroryId($id, $limit, $offset);
				return json_encode(array('data'=>true,'status'=>true,'value'=>$more_list_article));
			}			
		}
		public function article_related(){
			
			//$this->render('/article_detail');
		}
	}
?>