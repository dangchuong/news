<?php
	App::uses('AppController','Controller');
	//Home Controller
	class HomesController extends AppController
	{
		public $layout = 'default_front';
		public $uses = array('Article');		
		public function index(){			
			$articles = $this->Article->getArticle();
			$this->set('articles',$articles);
			$categories = $this->Categories->getAllCategores();
			// pr($categories);exit;
			$this->set('categories',$categories);
			
		}
		
	}
?>