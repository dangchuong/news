<?php
	App::uses('AppController','Controller');
	/**
	* 
	*/
	class UserAdminController extends AppController
	{
		public $helpers = array('Html','Form' ,'Session');
		public $components = array('Session','Paginator','RequestHandler');
		public $layout = 'default';
		public $uses = array('User');

		public function admin_dashboard(){			
			
		}		
		public function admin_login(){	
			if($this->Session->check('User')){
				$this->redirect('/admin/dashboard');
			}	
			$this->layout = 'default_login';
			if($this->request->isPost()){
				$this->autoRender = false;
				$email = $this->request->data['email'];			
				$password = $this->request->data['password'];
				$check = $this->User->getUserByEmailPass($email, $password);
				//pr($check);exit;
				if(empty($check)){					
					return json_encode(array('status'=>true, 'data'=>false));
				}else{
					$this->Session->write('User', $check);
					return json_encode(array('status'=>true, 'data'=>true));
				}
			}
		}
		public function admin_logout(){
			$this->Session->destroy();
			$this->redirect('/admin/login');
		}
		public function admin_user(){
			if($this->Session->check('User')){
				$user = $this->User->admin_user();
				$this->set('data',$user);
				
			}else{
				$this->redirect('/admin/login');
			}			
		}
		public function load_ajax_user($limit,$offset){
			$this->autoRender = false;
			$a = $this->User->getAllUser();
			$alluser = count($a);
			if($this->RequestHandler->isAjax()){
					$user = $this->User->admin_user($limit,$offset);
					return json_encode(array('data'=>true,'status'=>true,'val'=>$user,'total'=>$alluser));
				}
		}
		public function check_email($email){
			$this->autoRender = false;
			$check = $this->User->check_email($email);
			if(empty($check)){
				return json_encode(array('status'=>true,'data'=>true));
			}else{
				return json_encode(array('status'=>true,'data'=>false));
			}
		}
		public function admin_add_user(){
			if($this->request->isPost()){
				$this->User->create();
				$this->User->nickname = $this->request->data['nickname'];
				$this->User->gender = $this->request->data['gender'];
				$this->User->email = $this->request->data['email'];
				$this->User->password = md5($this->request->data['password']);
				$this->User->is_admin = $this->request->data['role'];
				define ('SITE_ROOT', realpath(dirname(__FILE__)));
				//pr($_FILES['avata']);exit;
				if($_FILES['avata']['name'] != NULL){
					$path = "/../webroot/uploads/avatas/"; 
	                $tmp_name = $_FILES['avata']['tmp_name'];
	                $name = $_FILES['avata']['name'];
	                $arrname = explode(".",$name);
	                $namemd5 = md5($arrname[0].date('Y-m-d H:i:s'));
	                $format = $arrname[1];		                
	                move_uploaded_file($tmp_name,SITE_ROOT.$path.$namemd5.".".$format);
	                $this->User->avata = "uploads/avatas/".$namemd5.".".$format;
				}
				if($this->User->save($this->User)){
					$this->redirect('/admin/user');
				}
			}
		}
		public function admin_article(){
			
		}
		public function admin_edit_user($id = null){	
			if($this->Session->check('User')){	
				$session = $this->Session->read('User')	;
				$id_session = $session['User']['id'];
				$user = $this->User->getUserById($id);
				$this->set('data',$user);
				if($this->request->isPost()){
					$this->User->id = $id;
					$this->User->nickname = $this->request->data['nickname'];
					$this->User->gender = $this->request->data['gender'];
					$this->User->email = $this->request->data['email'];
					$this->User->is_admin = $this->request->data['role'];
					//pr($_FILES['avata']['tmp_name']);exit;
					define ('SITE_ROOT', realpath(dirname(__FILE__)));
					if($_FILES['avata']['name'] != NULL){
						$path = "/../webroot/uploads/avatas/"; 
		                $tmp_name = $_FILES['avata']['tmp_name'];
		                $name = $_FILES['avata']['name'];
		                $arrname = explode(".",$name);
		                $namemd5 = md5($arrname[0].date('Y-m-d H:i:s'));
		                $format = $arrname[1]; 	                
		                move_uploaded_file($tmp_name,SITE_ROOT.$path.$namemd5.".".$format);
		                $this->User->avata = "uploads/avatas/".$namemd5.".".$format;
		                $old_avata = $this->request->data['old-avata'];
		                if($old_avata == ''){

		                }else{
		                	unlink($old_avata);
		                }		                
					}
					if($this->User->save($this->User)){	
						if($id == $id_session){
							$session = $this->User->getUserById($id_session);
							$this->Session->write('User',$session);
							$this->redirect('/admin/user');
						}else{
							$this->redirect('/admin/user');
						}						
					}
				}
			}else{
		 		$this->redirect('/admin/login');
		 	}						
		}
		
		public function admin_delete_user($id = null){
			$this->autoRender = false;
			$this->User->id = $id;
			$user = $this->User->getUserById($id);
			foreach ($user as $value) {
				$old_avata = $value['User']['avata'];
			}
			$this->User->delete($this->User->id);
			if($old_avata == ''){

            }else{
            	unlink($old_avata);
            }				
			$this->redirect('/admin/user');
		}
	}
?>