<?php
	App::uses('AppController','Controller');
	/**
	* 
	*/
	class ArticleAdminController extends AppController
	{	
		public $uses = array('Article','Categories');
		public $layout = 'default';
		public function admin_article(){
			if($this->Session->check('User')){
				$articles = $this->Article->getAllArticle();
				$this->set('data',$articles);
			}else{
				$this->redirect('/admin/login');
			}
		}
		public function admin_article_add(){
			if($this->Session->check('User')){
				$categories = $this->Categories->getAllCategores();
				$this->set('data',$categories);
				if($this->request->isPost()){
					$this->Article->create();					
					$this->Article->title = $this->request->data['title'];
					$this->Article->description = $this->request->data['description'];
					$this->Article->detail = $this->request->data['content'];
					$this->Article->category_id = $this->request->data['categories'];
					define ('SITE_ROOT', realpath(dirname(__FILE__)));
					if($_FILES['avata']['name'] != NULL){
						$path = "/../webroot/uploads/images/"; 
		                $tmp_name = $_FILES['avata']['tmp_name'];
		                $name = $_FILES['avata']['name'];
		                $arrname = explode(".",$name);
		                $namemd5 = md5($arrname[0].date('Y-m-d H:i:s'));
		                $format = $arrname[1]; 		                
		                move_uploaded_file($tmp_name,SITE_ROOT.$path.$namemd5.".".$format);
		                $this->Article->image = "uploads/images/".$namemd5.".".$format;
					}
					$this->Article->save($this->Article);
					$this->redirect('/admin/article');
				}			
			}else{
				$this->redirect('/admin/login');
			}
		}
		public function admin_article_detail($id = null) {
			if($this->Session->check('User')){
				$articles = $this->Article->getArticleById($id);
				$this->set('articles',$articles);
				
			}else{
				$this->redirect('/admin/login');
			}
		}

		public function admin_article_edit($id = null){
			if($this->Session->check('User')){
				$articles = $this->Article->getArticleById($id);
				$this->set('data',$articles);
				$categories2 = $this->Categories->getAllCategores();
				$this->set('data2',$categories2);
				if($this->request->isPost()){
					$this->Article->id = $id;
					$this->Article->category_id = $this->request->data['categories'];
					$this->Article->title = $this->request->data['title'];
					$this->Article->description = $this->request->data['description'];
					$this->Article->detail = $this->request->data['content'];					
					$this->Article->updated_at = date('Y-m-d H:i:s');
					// pr($this->Article->updated_at);exit;
					define ('SITE_ROOT', realpath(dirname(__FILE__)));
					if($_FILES['avata']['name'] != NULL){
						$path = "/../webroot/uploads/images/"; 
		                $tmp_name = $_FILES['avata']['tmp_name'];
		                $name = $_FILES['avata']['name'];
		                $arrname = explode(".",$name);
		                $namemd5 = md5($arrname[0].date('Y-m-d H:i:s'));
		                $format = $arrname[1]; 	                
		                move_uploaded_file($tmp_name,SITE_ROOT.$path.$namemd5.".".$format);
		                $this->Article->image = "uploads/images/".$namemd5.".".$format;
		                $old_avata = $this->request->data['old-avata'];
		                if($old_avata == ''){

		                }else{
		                	unlink($old_avata);
		                }		                
					}
					if($this->Article->save($this->Article)){					
						$this->redirect('/admin/article');
					}
				}
			}else{
				$this->redirect('/admin/login');
			}
		}
		public function admin_delete_article($id = null){
			$this->autoRender = false;
			$this->Article->id = $id;
			$article = $this->Article->getArticleById($id);
			foreach ($article as $value) {
				$old_avata = $value['Article']['image'];
			}
			$this->Article->delete($this->Article->id);
			if($old_avata == ''){

            }else{
            	unlink($old_avata);
            }				
			$this->redirect('/admin/article');
		}
	}
?>