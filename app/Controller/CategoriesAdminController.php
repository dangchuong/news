<?php
	App::uses('AppController','Controller');
	/**
	* 
	*/
	class CategoriesAdminController extends AppController
	{	
		public $helpers = array('Html','Form' ,'Session');
		public $components = array('Session','Paginator');
		public $layout = 'default';
		public $uses = array('Categories');
		public function admin_categories(){
			if($this->Session->check('User')){
				$categories = $this->Categories->getAllCategores();
				$this->set('data',$categories);
			}else{
				$this->redirect('/admin/login');
			}
		}
		public function check_name($name){
			$this->autoRender = false;
			$res = $this->Categories->getNameByName($name);
			if(empty($res)){
				return json_encode(array('status'=>true,'data'=>true));
			}else{
				return json_encode(array('status'=>true,'data'=>false));
			}
		}
		public function admin_categories_add(){
			if($this->request->isPost()){
				$this->Categories->name = $this->request->data['name'];
				if($this->Categories->save($this->Categories)){
					$this->redirect('/admin/categories');
				}
			}
		}
		public function admin_delete_cat($id){
			$this->autoRender = false;
			$this->Categories->id = $id;
			$this->Categories->delete($this->Categories->id);
			$this->redirect('/admin/categories');
		}
	}
?>