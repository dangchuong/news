<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	//Router::connect('/', array('controller' => 'admins', 'action' => 'admin_login'));
	Router::connect('/', array('controller' => 'Homes', 'action' => 'index'));
	Router::connect('/admin', array('controller' => 'UserAdmin', 'action' => 'admin_login'));
	Router::connect('/admin/login', array('controller' => 'UserAdmin', 'action' => 'admin_login'));
	Router::connect('/admin/logout', array('controller' => 'UserAdmin', 'action' => 'admin_logout'));
	Router::connect('/admin/dashboard', array('controller' => 'UserAdmin', 'action' => 'admin_dashboard'));
	Router::connect('/admin/user', array('controller' => 'UserAdmin', 'action' => 'admin_user'));
	Router::connect('/admin/user/add', array('controller' => 'UserAdmin', 'action' => 'admin_add_user'));
	Router::connect('/admin/article', array('controller' => 'ArticleAdmin', 'action' => 'admin_article'));
	Router::connect('/admin/article/add', array('controller' => 'ArticleAdmin', 'action' => 'admin_article_add'));
	Router::connect('/admin/categories', array('controller' => 'CategoriesAdmin', 'action' => 'admin_categories'));
	Router::connect('/admin/categories/add', array('controller' => 'CategoriesAdmin', 'action' => 'admin_categories_add'));
	Router::connect('/check_name/:name', array('controller' => 'CategoriesAdmin', 'action' => 'check_name'),
		array('pass'=>array('name')));
	Router::connect('/admin/categories/delete/:id',
		array('controller'=>'CategoriesAdmin','action'=>'admin_delete_cat'),
		array('pass'=>array('id')));
	Router::connect('/admin/user/edit/:id', 
	 	array('controller' => 'UserAdmin', 'action' => 'admin_edit_user'),
	 	array('pass' => array('id'))
	);
	Router::connect('/admin/load_more_user/:limit/:offset', 
	 	array('controller' => 'UserAdmin', 'action' => 'load_ajax_user'),
	 	array('pass' => array('limit','offset'))
	);
	Router::connect('/admin/article/detail/:id', 
	 	array('controller' => 'ArticleAdmin', 'action' => 'admin_article_detail'),
	 	array('pass' => array('id'))
	);
	Router::connect('/admin/article/edit/:id', 
	 	array('controller' => 'ArticleAdmin', 'action' => 'admin_article_edit'),
	 	array('pass' => array('id'))
	);
	Router::connect('/check_email/:email', 
	 	array('controller' => 'UserAdmin', 'action' => 'check_email'),
	 	array('pass' => array('email'))
	);
	Router::connect('/admin/user/delete/:id', 
		array('controller' => 'UserAdmin', 'action' => 'admin_delete_user'),
		array('pass' => array('id'))
	);
	Router::connect('/admin/article/delete/:id', 
		array('controller' => 'ArticleAdmin', 'action' => 'admin_delete_article'),
		array('pass' => array('id'))
	);
	Router::connect('/admin/categories/delete/:id', 
	 	array('controller' => 'Categories', 'action' => 'admin_ categories_delete'),
	 	array('pass' => array('id'))
	);
	//Article
	Router::connect('/article/detail/:id', array('controller' => 'Article', 'action' => 'article_detail'),
											array('pass'=>array('id')));
	//Load more list article
	Router::connect('/more_list_article/:id/:limit/:off', array('controller' => 'Article', 'action' => 'more_list_article'),
														array('pass'=>array('id','limit','off')));
	//Category
	Router::connect('/category/:id', array('controller' => 'Article', 'action' => 'list_article'),
											array('pass'=>array('id')));
	//Router::connect('/check_login/:email', array('controller' => 'admins', 'action' => 'check_login',array('pass' => array('email'))));
	//Router::connect('/admin', array('controller' => 'admins', 'action' => 'admin_index','admin' => true));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
