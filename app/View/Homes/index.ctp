<div id="body">
		<div id="home">
			
			<div class="content">
				<div>
					<h2>mới nhất</h2>
<?php if(!empty($articles)):?>
					<ul class="services">
<?php foreach($articles as $key => $value):?>
						<li>
							<a href="/article/detail/<?php echo $value['Article']['id']?>">								
								<span style="background: url(<?php echo $value['Article']['image']==''? 'uploads/images/no-img.jpg': $value['Article']['image'];?>);width:230px;height:150px;display: inline-block;background-position: center center;background-repeat: no-repeat;background-size: cover;"></span>
								<p>
									<?php echo $value['Article']['title'] ?>
								</p>
							</a>
							
						</li>
						
<?php endforeach;?>					
					</ul>
<?php endif;?>
				</div>
<?php if(!empty($categories)):?>
	<?php foreach($categories as $key => $value):?>
				
		<?php 
			//Load Model Article in View
			App::import('Model','Article');
			$articleModel = new Article();
			$get_article_by_category = $articleModel->getArticleByCateroryId($value['Categories']['id']);		
		?>
		<?php if(!empty($get_article_by_category)):?>

				<div class="post-ch">
					<h2 class="title-ch"><?php echo $value['Categories']['name'];?></h2>		
					<ul class="blog">
		<?php foreach($get_article_by_category as $k => $v):?>
						<li>
							<span><a href="/article/detail/<?php echo $v['Article']['id']?>">
								<span style="background: url(<?php echo $v['Article']['image']=='' ? '/uploads/images/no-img.jpg' : $v['Article']['image'];?>);width:100%;height:6.88vw;display: inline-block;background-position: center center;background-repeat: no-repeat;background-size: cover;"></span>
							</a></span>
							<div>
								<h3 style="font-weight:bold;font-size:18px;"><a href="/article/detail/<?php echo $v['Article']['id']?>"><?php echo $v['Article']['title'];?></a></h3>
								<span class="date" style="font-size:12px; color:green;font-weight:bold;"><?php echo $v['Article']['created_at'];?></span>
								<p>
									<?php echo $v['Article']['description'];?>
								</p>
							</div>
						</li>
		<?php endforeach;?>
					</ul>
				<?php if($k >= 2):?>
					<a href="/category/<?php echo $value['Categories']['id']?>" class="more" style="color:blue;font-size:20px;">Xem thêm</a>
				<?php endif;?>
				</div>
		<?php endif;?>
					
				
	<?php endforeach;?>
<?php endif;?>
			</div>
			<div class="sidebar">
				<div>
					<h2>About</h2>
					<img src="front-end/images/people.jpg" alt="Image">
					<h3>This is just a place holder, so you can see what the site would look like.</h3>
					<p>
						Aenean at nisl fringilla purus lacinia facilisis at vitae lectus. Nam varius posuere velit, nec blandit diam vehicula quis. Aliquam erat volutpat.
					</p>
				</div>
				<div>
					<h2>Testimonials</h2>
					<ul>
						<li>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a tortor sapien. In molestie, velit vitae luctus dapibus - <span>Happy Client</span>
							</p>
						</li>
						<li>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a tortor sapien. In molestie, velit vitae luctus dapibus - <span>Satisfied Customer</span>
							</p>
						</li>
						<li>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a tortor sapien. In molestie, velit vitae luctus dapibus - <span>Entrepreneur</span>
							</p>
						</li>
					</ul>
					<a href="index.html" class="more">View all</a>
				</div>
				<div>
					<h2>Newsletter</h2>
					<form action="index.html">
						<input type="text" value="Enter Email Address" id="email">
						<input type="submit" value="Ok" id="submit">
					</form>
				</div>
			</div>
		</div>
	</div>