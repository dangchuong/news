<div id="body">
	<div class="post-ch">
		<h2 class="title-ch"><?php echo $title['Categories']['name'];?></h2>
		<ul class="blog">
<?php foreach($list_articles as $k => $v):?>		
			<li style="clear:both; margin-bottom:20px;overflow:auto;">
				<span><a href="/article/detail/<?php echo $v['Article']['id']?>">
								<span style="background: url(/<?php echo $v['Article']['image']=='' ? 'uploads/images/no-img.jpg' : $v['Article']['image'];?>);width:20%;height:6.88vw;display: inline-block;background-position: center center;background-repeat: no-repeat;background-size: cover;float:left;margin-right:10px;"></span>
							</a></span>
				<div style="overflow:hidden;">
					<h3 style="font-weight:bold;font-size:18px;"><a href="/article/detail/<?php echo $v['Article']['id']?>"><?php echo $v['Article']['title']?></a></h3>
					<span class="date" style="font-size:12px; color:green;font-weight:bold;"><?php echo $v['Article']['created_at']?></span>
					<p>
						<?php echo $v['Article']['description']?>
					</p>
				</div>
			</li>	
<?php endforeach;?>	
		</ul>
		
		<p class="more ch-more js-more-list-article" data-id = "<?php echo $id;?>">Xem thêm</p>
			
	</div>
</div>
<script id="tmpl-more-list-article" type="text/x-jquery-tmpl">
	<li style="clear:both; margin-bottom:20px;overflow:auto;">
		<span><a href="/article/detail/${Article.id}">
						<span style="background: url(/{{if Article.image == ''}}uploads/images/no-img.jpg{{else}}${Article.image}{{/if}});width:20%;height:6.88vw;display: inline-block;background-position: center center;background-repeat: no-repeat;background-size: cover;float:left;margin-right:10px;"></span>
					</a></span>
		<div style="overflow:hidden;">
			<h3 style="font-weight:bold;font-size:18px;"><a href="/article/detail/${Article.id}">${Article.title}</a></h3>
			<span class="date" style="font-size:12px; color:green;font-weight:bold;">${Article.created_at}</span>
			<p>
				${Article.description}
			</p>
		</div>
	</li>
</script>