<div class="box box-primary">
              <form role="form" id="f-add-article" action="/admin/article/add/" method="POST" enctype="multipart/form-data">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Article</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
             
                
                  <div class="box-body">
                  
                      <div class="form-group">
                      
                      <label for="exampleInputEmail1">Tiêu đề</label>
                      <input type="text" class="form-control js-title" id="exampleInputEmail1" placeholder="Tiêu đề bài viết" value="" name="title">
                      <span class="error-msg error-nick-name"></span>
                    </div>
                    <label for="exampleInputEmail1">Mô tả</label>
                    <textarea class="form-control js-description" rows="3" placeholder="Enter ..." name="description"></textarea>
                    <span class="error-msg error-email"></span>
                    <label for="exampleInputEmail1">Nội dung</label>
                    <section class="content">
			          <div class="row">
			            <div class="col-md-12">
			                    <textarea id="editor1" name="content" rows="10" cols="80" class="js-content ckeditor">

			                    </textarea>
                          
			            </div><!-- /.col-->

			          </div><!-- ./row -->
                <span class="error-msg error-password"></span>
			        </section><!-- /.content -->
	                <div class="form-group">
                      <label for="exampleInputFile">Ảnh</label>
                      <input type="file" id="exampleInputFile" name="avata">
                      <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <label for="exampleInputEmail1">Categories</label></br>
                    <div class="form-group">
                      
                      <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="categories">
                      <?php
                        foreach ($data as $val) {
                      ?>
                          <option value="<?php echo $val['Categories']['id'];?>"><?php echo $val['Categories']['name'];?></option>
                      <?php
                        }
                      ?>
                       
                        
                      </select><span class="select2 select2-container select2-container--default select2-container--below" dir="ltr" style="width: 637px;"><span class="selection"></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                  </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-primary" id="js-add-btn-art">Add</button>
                  </div>
                </form>
            
              </div>
