
<h3 class="box-title">Users </h3>
<a href="/admin/user/add">Add User</a>
<div class="box-body">
  <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable jsTr" role="grid" aria-describedby="example2_info">
    <thead>
      <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">ID</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Is_active</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Is_admin</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Nickname</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Gender</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Avata</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Account_type</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Email</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Create_at</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Update_at</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="2" aria-label="CSS grade: activate to sort column ascending">Action</th></tr>
    </thead>
    <tbody>     
    <?php foreach ($data as $value):?>                   
    <tr role="row" class="odd">
        <td class="sorting_1"><?php echo $value['User']['id'];?></td>
        <td><?php echo $value['User']['is_active'];?></td>
        <td><?php echo $value['User']['is_admin'];?></td>
        <td><?php echo $value['User']['nickname'];?></td>
        <td><?php echo $value['User']['gender'];?></td>
        <td><img src="/<?php echo $value['User']['avata'] == '' ? 'uploads/avatas/photo.jpg' : $value['User']['avata'];?>" class="avata-img"></td>
        <td><?php echo $value['User']['account_type'];?></td>
        <td><?php echo $value['User']['email'];?></td>
        <td><?php echo $value['User']['created_at'];?></td>
        <td><?php echo $value['User']['updated_at'];?></td>
        <td><a href="/admin/user/edit/<?php echo $value['User']['id'];?>">Edit</a></td>
        <td><a href="/admin/user/delete/<?php echo $value['User']['id'];?>">Delete</a></td>
    </tr>
    <?php endforeach;?>
    </tbody>
    <tfoot>
      <tr><th rowspan="1" colspan="1">ID</th><th rowspan="1" colspan="1">Is_active</th><th rowspan="1" colspan="1">Is_admin</th><th rowspan="1" colspan="1">Nickname</th><th rowspan="1" colspan="1">Gender</th><th rowspan="1" colspan="1">Avata</th><th rowspan="1" colspan="1">Account_type</th><th rowspan="1" colspan="1">Email</th><th rowspan="1" colspan="1">Create_at</th><th rowspan="1" colspan="1">Update_at</th><th rowspan="1" colspan="2">Action</th></tr>
    </tfoot>
  </table></div></div><!-- <div class="row"><div class="col-sm-5"><div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example2_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example2_previous"><a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example2" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="example2_next"><a href="#" aria-controls="example2" data-dt-idx="7" tabindex="0">Next</a></li></ul></div></div></div> --></div>

  <p class="loadmore jsMore">Xem thêm</p>
  <img src="/assets/img/loading.gif" class="load-gif jsLoading">  
</div>
<script id="user_tmpl" type="text/x-jquery-tmpl">
    <tr>
        <td>${User.id}</td>
        <td>${User.is_active}</td>
        <td>${User.is_admin}</td>
        <td>${User.nickname}</td>
        <td>${User.gender}</td>
        
        <td><img src="/{{if User.avata == ''}}uploads/avatas/photo.jpg{{else}}${User.avata}{{/if}}" class="avata-img"></td>
        <td>${User.account_type}</td>
        <td>${User.email}</td>
        <td>${User.created_at}</td>
        <td>${User.updated_at}</td>
        <td><a href="/admin/user/edit/${User.id}">Edit</a></td>
        <td><a href="/admin/user/delete/${User.id}">Delete</a></td>
    </tr>
  </script>