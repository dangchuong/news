<div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add User</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
             
                <form role="form" id="f-add-user" action="/admin/user/add/" method="POST" enctype="multipart/form-data">
                  <div class="box-body">
                  
                      <div class="form-group">
                      
                      <label for="exampleInputEmail1">Nick name</label>
                      <input type="text" class="form-control js-nick-name" id="exampleInputEmail1" placeholder="Enter nick name" value="" name="nickname">
                      <span class="error-msg error-nick-name"></span>
                    </div>
                    <div class="form-group">
                      <label>Giới tính</label>
                      <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="gender">
                        <option value="1">Nam</option>
                        <option value="0">Nữ</option>
                      </select><span class="select2 select2-container select2-container--default select2-container--below" dir="ltr" style="width: 637px;"><span class="selection"></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputFile">Avata</label>
                      <input type="file" id="exampleInputFile" name="avata">
                      <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="email" class="form-control js-email" id="exampleInputEmail1" placeholder="Enter email" value="" name="email">
                      <span class="error-msg error-email"></span>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Password</label>
                      <input type="password" class="form-control js-password" id="exampleInputEmail1" placeholder="Enter password" value="" name="password">
                      <span class="error-msg error-password"></span>
                    </div>
                    <div class="form-group">
                      <label>Quyền</label>
                      <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="role">
                        <option value="1">Admin</option>
                        <option value="0">User</option>
                      </select><span class="select2 select2-container select2-container--default select2-container--below" dir="ltr" style="width: 637px;"><span class="selection"></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                  </div>
               
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" class="btn btn-primary" id="js-add-btn-f">Add</button>
                  </div>
                </form>
            
              </div>