<div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit User</h3>
                </div><!-- /.box-header -->
         
             			
             	
                <form role="form" id="f-edit-user" action="/admin/user/edit/<?php echo $data['User']['id'];?>" method="POST" enctype="multipart/form-data">
                  <div class="box-body">
                  
                  		<div class="form-group">
                  		<input type="hidden" name="id" value="<?php echo $data['User']['id'];?>">
                      <label for="exampleInputEmail1">Nick name</label>
                      <input type="text" class="form-control js-nick-name" id="exampleInputEmail1" placeholder="Enter email" value="<?php echo $data['User']['nickname'];?>" name="nickname">
                      <span class="error-msg error-nick-name"></span>
                    </div>
                    <div class="form-group">
	                    <label>Giới tính</label>
	                    <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="gender">
	                      <?php
	                      	$gender = array('1'=>'Nam','0'=>'Nữ');
	                      	foreach ($gender as $key => $value) {
	                      ?>
	                      		<option value="<?php echo $key;?>" <?php echo $data['User']['gender'] == $key ? 'selected':''?>><?php echo $value?></option>
	                      <?php 
	                      	}
	                      ?>
	                      
	                    </select><span class="select2 select2-container select2-container--default select2-container--below" dir="ltr" style="width: 637px;"><span class="selection"></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
	                </div>
	                <div class="form-group">
                      <label for="exampleInputFile">Avata</label>
                      <input type="file" id="exampleInputFile" name="avata">
                      <input type="hidden" value="<?php echo $data['User']['avata'];?>" name="old-avata"> 
                      <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="email" class="form-control js-email" id="exampleInputEmail1" placeholder="Enter email" value="<?php echo $data['User']['email'];?>" name="email">
                      <span class="error-msg error-email"></span>
                    </div>
                    <div class="form-group">
	                    <label>Quyền</label>
	                    <select class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="role">
	                      <?php
	                      	$role = array('1'=>'Admin','0'=>'User');
	                      	foreach ($role as $key => $value) {
	                      ?>
	                      		<option value="<?php echo $key;?>" <?php echo $data['User']['is_admin'] == $key ? 'selected':''?>><?php echo $value?></option>
	                      <?php 
	                      	}
	                      ?>
	                      
	                    </select><span class="select2 select2-container select2-container--default select2-container--below" dir="ltr" style="width: 637px;"><span class="selection"></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
	                </div>
	             
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" class="btn btn-primary" id="js-sm-btn-f">Update</button>
                  </div>
                </form>
          
                    
              </div>