<div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Categories</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
             
                <form role="form" id="f-add-category" action="/admin/categories/add/" method="POST" enctype="multipart/form-data">
                  <div class="box-body">
                  
                      <div class="form-group">
                      
                      <label for="exampleInputEmail1">Category Name</label>
                      <input type="text" class="form-control js-name" id="exampleInputEmail1" placeholder="Enter category name" value="" name="name">
                      <span class="error-msg error-name"></span>
                    </div>   
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" class="btn btn-primary" id="js-add-category-f">Add</button>
                  </div>
                </form>
            
              </div>