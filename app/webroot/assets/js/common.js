//===========login===================
$('#js-sm-login').click(function(){
	var email = $('#js-valid-email').val();
	if(email == ''){
		$('.error-mail').text('xin hãy nhập email');
		return false;
	}else{
		$('.error-mail').text('');
	}
	var pass = $('#js-valid-pass').val();
	if(pass == ''){
		$('.error-pass').text('xin hãy nhập mật khẩu');
		return false;
	}else{
		$('.error-pass').text('');
	}

	$.ajax({
		url:'/admin/login',
		type:'post',
		data: $('#form-login').serialize(),
		dataType:'json',
		success: function(e){
			if(e.status){
				if(e.data){
					window.location.href = '/admin/dashboard';
					//alert("aa");
				}else{
					$('.error-mail').text('sai email va password');
					return false;
				}
			}
		}
	});
});
//====== Validate Add user ==============
$('#js-add-btn-f').click(function(){
	var nickname = $('.js-nick-name').val();
	var email = $('.js-email').val();
	var password = $('.js-password').val();
	if(nickname == ''){
		$('.error-nick-name').text('hãy nhập nickname');
		return false;
	}else{
		$('.error-nick-name').text('');
	}
	if(email == ''){
		$('.error-email').text('hãy nhập email');
		return false;
	}else{
		$('.error-email').text('');
	}
	if(password == ''){
		$('.error-password').text('hãy nhập password');
		return false;
	}else{
		$('.error-password').text('');
		//$('#f-add-user').submit();
	} 
	$.ajax({
		url:'/check_email/'+email,
		dataType:'JSON',
		type: 'POST',
		success: function(e){
			if(e.status){
				if(e.data){
					$('#f-add-user').submit();
				}else{
					$('.error-email').text('Email đã tồn tại');
					return false;
				}
			}
		}
	});
});

//====== Validate Edit user ==============
$('#js-sm-btn-f').click(function(){
	var nickname = $('.js-nick-name').val();
	var email = $('.js-email').val();
	
	if(nickname == ''){
		$('.error-nick-name').text('hãy nhập nickname');
		return false;
	}else{
		$('.error-nick-name').text('');
	}
	if(email == ''){
		$('.error-email').text('hãy nhập email');
		return false;
	}else{
		$('.error-email').text('');
		$('#f-edit-user').submit();
	}
	
	// $.ajax({
	// 	url:'/check_email/'+email,
	// 	dataType:'JSON',
	// 	type: 'POST',
	// 	success: function(e){
	// 		if(e.status){
	// 			if(e.data){
					
	// 			}else{
	// 				$('.error-email').text('Email đã tồn tại');
	// 				return false;
	// 			}
	// 		}
	// 	}
	// });
});

//====== Validate Add Article ==============
$('#js-add-btn-art').click(function(){
	var title = $('.js-title').val();
	var description = $('.js-description').val();
	var content = $('.js-content').val();
	//console.log(content);
	if(title == ''){
		$('.error-nick-name').text('hãy nhập title');
		return false;
	}else{
		$('.error-nick-name').text('');
	}
	if(description == ''){
		$('.error-email').text('hãy nhập mô tả');
		return false;
	}else{
		$('.error-email').text('');
	}
	if(content == ''){
		$('.error-password').text('hãy nhập nội dung');
		return false;
	}else{
		$('.error-password').text('');
		$('#f-add-article').submit();
	} 
	
});
$('#js-add-category-f').click(function(){
	var name = $('.js-name').val();
	if(name == ''){
		$('.error-name').text('ko được để trống');
		return false;
	}else{
		$('.error-name').text('');
	}
	$.ajax({
		url: '/check_name/'+name,
		dataType: 'JSON',
		type: 'POST',
		success: function(e){
			if(e.status){
				if(e.data){
					$('.error-name').text('');
					$('#f-add-category').submit();
				}else{
					$('.error-name').text('trùng category');
					return false;
				}
			}
		}
	});
});
/*------------ load more users---------------*/
$('.jsLoading').hide();
var _offset = 2;
$('.jsMore').click(function(){
	$('.jsLoading').show();
	$('.jsMore').hide();
	var _limit = 3;
	_offset = _offset;
	var _url = '/admin/load_more_user/'+_limit+'/'+_offset;
	$.ajax({
		url: _url,
		type: 'POST',
		dataType: 'JSON',
		success: function(res){
			if(res.data){
				if(res.status){
					$.each(res.val, function(key, value){						
						$('#user_tmpl').tmpl(value).appendTo('.jsTr');
					});
					var _total = res.total;
					_offset+=_limit;
					if(_offset >= _total){
						$('.jsMore').hide();
						$('.jsLoading').hide();
					}else{
						$('.jsLoading').hide();
						$('.jsMore').show();
					}					
				}
			}
		}
	});
});
/*------------ end load more users---------------*/

/*------------ load more list article---------------*/
	
	var _off = 2;
$('.js-more-list-article').click(function(){
	var _id = $('.js-more-list-article').attr('data-id');
	var _limit = 1;
	_off = _off;
	$.ajax({
		url:'/more_list_article/'+_id+'/'+_limit+'/'+_off,
		type:'POST',
		dataType:'JSON',
		success: function(res){
			if(res.data){
				if(res.status){
					$.each(res.value, function(key, val){
						$('#tmpl-more-list-article').tmpl(val).appendTo('ul.blog');						
					});
					_off+=_limit;					
				}
			}
		}
	});
});
/*------------ end load more list article---------------*/