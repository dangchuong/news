<?php 
	App::uses('AppModel','Model');
	/**
	* 
	*/
	class User extends AppModel
	{ 
		public $useTable = 'users';
		public function admin_user($limit = 2,$offset = 0){
			return $this->find('all',array('limit'=>$limit,'offset'=>$offset));
		}
		public function getAllUser(){
			return $this->find('all');
		}
		public function getUserByEmail($email){
			return $this->find('all',array('conditions'=>array('email'=>$email)));			
		}	
		public function getUserByEmailPass($email, $pass){
			return $this->find('first',array('conditions'=>array('email'=>$email,'password'=>md5($pass))));			
		}	
		public function getUserById($id){
			return $this->find('first',array('conditions'=>array('id'=>$id)));
		}	
		public function check_email($email){
			return $this->find('first',array('conditions'=>array('email'=>$email)));
		}
	}
?>