<?php 
	App::uses('AppModel','Model');
	/**
	* 
	*/
	class Categories extends AppModel
	{
		public $useTable = 'categories';
		public function getAllCategores(){
			return $this->find('all');
		}
		public function getCategoresById($id){
			return $this->find('all',array('conditions'=>array('id'=>$id)));
		}
		public function getNameByName($name){
			return $this->find('first',array('conditions'=>array('name'=>$name)));	
		}
		public function getNameCategoryById($id){
			return $this->find('first',array('fields'=>array('name'),
											 'conditions'=>array('id'=> $id)));
		}
	}
?>