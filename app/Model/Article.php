<?php 
	App::uses('AppModel','Model');
	/**
	* 
	*/
	class Article extends AppModel
	{
		public $useTable = 'articles';
		public function getAllArticle(){
			return $this->find('all',array('order'=> 'created_at DESC'));
		}
		public function getArticle($limit = 4, $offset = 0){
			return $this->find('all',array('limit'=>$limit,'offset'=>$offset,'order'=>'created_at DESC'));
		}
		public function getArticleById($id){
			
			return $this->find('first',array('conditions'=>array('id'=>$id)));
		}
		public function getArticleByCateroryId($id, $limit = 3, $offset = 0){
			return $this->find('all', array('conditions'=>array('category_id'=> $id),
											'limit'=>$limit,
											'offset'=>$offset,
											'order'=>'created_at DESC'));
		}
		public function getArticleRelated($category_id){			
			return $this->find('all',array(
					'fields'=>array('Article.title','Article.id'),
					'joins'=>array(
						array(
							'type' => 'INNER',
							'table' => 'categories',
							'alias' => 'Cate',
							'conditions' => 'Cate.id = Article.category_id'
						)
					),
					'conditions' => array(
						'Article.category_id' => $category_id
					),
					'order'=>array('Article.created_at'=>'DESC'),
				));
		}
		
	}
?>